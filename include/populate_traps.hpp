// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef POPULATE_TRAPS_HPP
#define POPULATE_TRAPS_HPP

namespace populate_traps
{
void populate_std_lvl();

}  // namespace populate_traps

#endif  // POPULATE_TRAPS_HPP
